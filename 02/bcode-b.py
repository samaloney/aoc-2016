#test_data = '''ULL
#RRDDD
#LURDL
#UUUUD'''

#instructions = test_data.splitlines()

with open('input.txt') as file:
    instructions = file.readlines()


bounds = ((2,2), (1,3), (0,4), (1,3), (2,2))

def follow_commands(start_location, commands):
    #print(commands)
    #print(start_location)
    current_location=start_location[:]
    for command in commands:
        if command == 'U':
            if current_location[1] != bounds[current_location[0]][0]:
                current_location[1] -= 1
        elif command == 'R':
            if current_location[0] != bounds[current_location[1]][1]:
                current_location[0] += 1
        elif command == 'D':
            if current_location[1] != bounds[current_location[0]][1]:
                current_location[1] += 1
        elif command == 'L':
            if current_location[0] != bounds[current_location[1]][0]:
                current_location[0] -= 1
        
        #print(current_location)
    return current_location

codes = []

for i, instruction in enumerate(instructions):
    print(i, instruction)
    if len(codes) == 0:
        new_code = follow_commands([0, 2], instruction)
    else:
        new_code = follow_commands(codes[i-1], instruction)
        
    codes.append(new_code)
    print(codes)

keys = [['-','-',1,'-', '-'], 
        ['-',2,3,4, '-'],
        [5, 6, 7, 8, 9],
        ['-','A','B','C', '-'],
        ['-','-','D','-', '-']]

for code in codes:
    print(keys[code[1]][code[0]])
