from collections import defaultdict

with open('input.txt') as file:
    data = file.read().rstrip('\n')

instructions = data.split(', ')

current_direction = 0
x_position = 0
y_position = 0
locations = defaultdict(int)

for instruction in instructions:
    if instruction[0] == 'R':
        current_direction = (current_direction + 90) % 360
    elif instruction[0] == 'L':
        current_direction = (current_direction - 90) % 360
    for step in range(int(instruction[1:])): 
        if current_direction == 0:
            y_position += 1
        elif current_direction == 90:
            x_position += 1
        elif current_direction == 180:
            y_position -= 1
        elif current_direction == 270:
            x_position -= 1
 
        key = '%s,%s' % (x_position, y_position)

        locations[key] += 1
        if locations[key] > 1:
            print(key)
            break

print(abs(x_position) + abs(y_position))
