from collections import defaultdict

with open('input.txt') as file:
    data = file.read().rstrip('\n')

instructions = data.split(', ')

current_direction = 0
x_position = 0
y_position = 0
locations = defaultdict(int)

print(x_position, y_position)

for instruction in instructions:
    if instruction[0] == 'R':
        current_direction = (current_direction + 90) % 360
    elif instruction[0] == 'L':
        current_direction = (current_direction - 90) % 360
   
    if current_direction == 0:
        y_position = y_position + int(instruction[1:])
    elif current_direction == 90:
        x_position = x_position + int(instruction[1:])
    elif current_direction == 180:
        y_position = y_position - int(instruction[1:])
    elif current_direction == 270:
        x_position = x_position - int(instruction[1:])
 
    print(instruction, current_direction, x_position, y_position)
    print(x_position, y_position) 
    key = '%s,%s' % (x_position, y_position)

print(abs(x_position) + abs(y_position))
