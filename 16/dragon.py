
def dragon_data(a):
    b = a[::-1]
    b = b.replace('0', '2')
    b = b.replace('1', '0')
    b = b.replace('2', '1')
    return a+'0'+b

def check_sum(a):
    out = []
    for i in range(0,len(a), 2):
        if a[i:i+1] == a[i+1:i+2]:
            out.append('1')
        else:
            out.append('0')

    return ''.join(out)

def check(a):
    cs = check_sum(a)
    while len(cs) % 2 == 0:
        cs = check_sum(cs)

    return cs

data = '11011110011011101'
while len(data) < 35651584:
    data = dragon_data(data)

gd  = data[:35651584]

cs = check(gd)

print(cs)
