from hashlib import md5

id = 'ugkcyxxp'

def pwdhash1(seed, length):
    counter = 0
    password = []
    while len(password) < length:
        tohash = (seed+str(counter)).encode()        
        pwdhex = md5(tohash).hexdigest()

        #if counter % 100000 == 0:
        #    print(tohash, pwdhex, counter)

        if pwdhex[0:5] == '0'*5:
            print(tohash, pwdhex, counter)
            password.append(pwdhex[5])
        
        counter += 1

    return ''.join(password)

#print(pwdhash(id, 8))

def pwdhash2(seed, length):
    counter = 0
    chars = 0
    password = list('_'*length)
    while chars < length:
        tohash = (seed+str(counter)).encode()        
        pwdhex = md5(tohash).hexdigest()

        if pwdhex[0:5] == '0'*5:
            if ord(pwdhex[5]) < ord(str(length)):
                print(tohash, pwdhex, ''.join(password))
                if password[int(pwdhex[5])] == '_': 
                    password[int(pwdhex[5])] = pwdhex[6]
                    chars += 1 
        
        counter += 1

    return ''.join(password)

print(pwdhash2(id, 8))
