
with open('input.txt') as f:
    data = f.readlines()


data = data[2:]

nodes = {}

for line in data:
    parts = line.split()
    node_x, node_y = parts[0].split('/')[-1].split('-')[-2:]
    node_id = (int(node_x[1:]), int(node_y[1:]))
    
    s, u, a = int(parts[1][:-1]), int(parts[2][:-1]), int(parts[3][:-1])

    nodes[node_id] = {'s':s, 'u':u, 'a':a}
    
pairs = 0
nodes= sorted(nodes.items(), key=lambda x: (x[1]['a'], x[1]['u']))


while len(nodes) > 1:
    cur_id, cur_prop = nodes.pop(0)
    if cur_prop['u'] != 0:
        for node_id, node_prop in nodes:
            if cur_prop['u'] <= node_prop['a']:
                pairs += 1

print(pairs)
