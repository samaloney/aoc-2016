
with open('input.txt') as f:
    data = f.readlines()

#data = '''/dev/grid/node-x0-y0   10T    8T     2T   80%
#/dev/grid/node-x0-y1   11T    6T     5T   54%
#/dev/grid/node-x0-y2   32T   28T     4T   87%
#/dev/grid/node-x1-y0    9T    7T     2T   77%
#/dev/grid/node-x1-y1    8T    0T     8T    0%
#/dev/grid/node-x1-y2   11T    7T     4T   63%
#/dev/grid/node-x2-y0   10T    6T     4T   60%
#/dev/grid/node-x2-y1    9T    8T     1T   88%
#/dev/grid/node-x2-y2    9T    6T     3T   66%'''

data = data[2:]
#data = data.split('\n')

nodes = {}
empty = 0

for line in data:
    parts = line.split()
    node_x, node_y = parts[0].split('/')[-1].split('-')[-2:]
    node_id = (int(node_x[1:]), int(node_y[1:]))
    
    s, u, a = int(parts[1][:-1]), int(parts[2][:-1]), int(parts[3][:-1])
    if u == 0:
        empty = node_id
    
    nodes[node_id] = {'s':s, 'u':u, 'a':a}
    
    
fixed = []
for k, v, in nodes.items():
    if v['u'] > nodes[empty]['s']:
        fixed.append(k)


size = max(nodes.keys(), key=lambda x: (x[1], x[0]))
 
d = [[ '.' for x in range(size[0]+1)] for y in range(size[1]+1)]
d[0][0] = 'G'
d[empty[1]][empty[0]] = '_'
for f in fixed:
    d[f[1]][f[0]] = '#'

for x in d: print(''.join(x))
print(empty[0] + empty[1] + size[0] + (size[0]-1)*5)

