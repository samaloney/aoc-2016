import itertools

data = '''swap position 4 with position 0
swap letter d with letter b
reverse positions 0 through 4
rotate left 1 step
move position 1 to position 4
move position 3 to position 0
rotate based on position of letter b
rotate based on position of letter d'''


def process(inst, ps):
    parts = inst.split(' ')
    if parts[0] == 'swap' and parts[1] == 'position':
        tmp = ps[int(parts[2])]
        ps[int(parts[2])] = ps[int(parts[5])]
        ps[int(parts[5])] = tmp
    elif parts[0] == 'swap' and parts[1] == 'letter':
        p1 = ps.index(parts[2])
        p2 = ps.index(parts[5])
        tmp = ps[p1]
        ps[p1] = ps[p2]
        ps[p2] = tmp
    elif parts[0] == 'rotate' and parts[1] != 'based':
        if parts[1] == 'right':
            ps = ps[-int(parts[2]):] + ps[:-int(parts[2])]
        elif parts[1] == 'left':
            ps = ps[int(parts[2]):] + ps[:int(parts[2])]
    elif parts[0] == 'rotate' and parts[1] == 'based':
        n = ps.index(parts[6])
        if n >= 4:
            n += 1
        n += 1
        for i in range(n):
            ps = ps[-1:] + ps[:-1]
    elif parts[0] == 'reverse':
        ps[int(parts[2]):int(parts[4])+1] = reversed(ps[int(parts[2]):int(parts[4])+1])
    elif parts[0] == 'move':
        tmp = ps.pop(int(parts[2]))
        ps.insert(int(parts[5]), tmp)

    return ps

with open('input.txt') as file:
    data = file.read()

lines = data.split('\n')

ps = list('abcdefgh')

def scramble(word):
    ps = list(word)
    for line in lines:
        ps = process(line, ps)
    
    return ps

print(''.join(scramble(ps)))

for perm in itertools.permutations('abcdefgh', 8):
    scram = scramble(''.join(perm))
    if scram == ['f', 'b', 'g', 'd', 'c', 'e', 'a', 'h']:
        break

print(''.join(perm))
