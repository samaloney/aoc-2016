data = '''cpy 2 a
tgl a
tgl a
tgl a
cpy 1 a
dec a
dec a'''


with open('input.txt') as file:
    data = file.read()


registers = {'a': 7, 'b': 0, 'c': 0, 'd': 0}

sc = '''cpy b c
inc a
dec c
jnz c -2
dec d
jnz d -5'''.split('\n')

def exec_instrs(data):
    cur_instr = 0
    instrs =  data.split('\n')
    instrs = instrs[:-1]
    while cur_instr < len(instrs):
        #print(cur_instr, instrs[cur_instr], registers)
        parts = instrs[cur_instr].split(' ')
        cmd = parts[0]
        
        cur_instr += 1
        if instrs[cur_instr-1:cur_instr - 1 + 6] == sc:
            registers['a'] += registers['d'] * registers['b']
            registers['d'] = 0
            registers['c'] = 0
            cur_instr += 5          
        elif cmd == 'cpy':
            if parts[2] in ['a', 'b', 'c', 'd']:
                if parts[1] in ['a', 'b', 'c', 'd']:
                    registers[parts[2]] = registers[parts[1]]
                else:
                    registers[parts[2]] = int(parts[1])
            
        elif cmd == 'inc':
            registers[parts[1]] += 1
        elif cmd == 'dec':
            registers[parts[1]] -= 1
        elif cmd == 'jnz':
            if parts[1] in ['a', 'b', 'c', 'd']:
                if registers[parts[1]] != 0:
                    if parts[2] in ['a', 'b', 'c', 'd']:
                        cur_instr = (cur_instr - 1) + registers[parts[2]]
                    else:
                        cur_instr = (cur_instr - 1) + int(parts[2])
            else:
                if parts[1] != '0':
                    if parts[2] in ['a', 'b', 'c', 'd']:
                        cur_instr = (cur_instr - 1) + registers[parts[2]]
                    else:
                        cur_instr = (cur_instr - 1) + int(parts[2])

        elif cmd == 'tgl':
            if parts[1] in ['a', 'b', 'c', 'd']:
                i = registers[parts[1]]
            else:
                i = int(cmd[1])

            if (cur_instr + i -1) < len(instrs):
                p = instrs[cur_instr + i -1]. split()
                if len(p) == 2:
                    if p[0] == 'inc':
                        instrs[cur_instr + i - 1] = 'dec ' + p[1]
                    else:
                        instrs[cur_instr + i - 1] = 'inc ' + p[1]
                else:
                    inst = ''
                    if p[0] == 'jnz':
                        inst = 'cpy ' + p[1] + ' ' + p[2]
                    else:
                        inst = 'jnz ' + p[1] + ' ' + p[2]
                    instrs[cur_instr + i - 1] = inst
                    

registers = {'a': 7, 'b': 0, 'c': 0, 'd': 0}
exec_instrs(data)
print(registers['a'])


registers = {'a': 12, 'b': 0, 'c': 0, 'd': 0}
exec_instrs(data)
print(registers['a'])
