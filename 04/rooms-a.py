from collections import defaultdict, OrderedDict

import re

test_data = '''aaaaa-bbb-z-y-x-123[abxyz]
a-b-c-d-e-f-g-h-987[abcde]
not-a-real-room-404[oarel]
totally-real-room-200[decoy]'''

with open('input.txt') as file:
    data = file.read()

pattern = r'(.+)-(\d+)\[(\w+)\]'

rooms = re.findall(pattern, data)

def count_chars(string):
    char_counts = OrderedDict()
    for char in string:
        if char in char_counts:
            char_counts[char] += 1
        else:
            char_counts[char] = 1        
    
    ck = sorted(char_counts.items(), key=lambda kv: (-kv[1], kv[0]))
    return ''.join([c[0] for c in ck])

def rotate(orig, key):
    out = []
    words = orig.split('-')
    for word in words:
        codes = [((ord(char)-97) + key) % 26  for char in word]
        word = [chr(code+97) for code in codes]
        out.append(''.join(word))

    return ' '.join(out)

total = 0

for room in rooms:
    checksum = count_chars(room[0].replace('-', ''))
    #print(checksum[:5], room[2], room[1])
    if checksum[:5] == room[2]:
        total += int(room[1])
    
    room_name = rotate(room[0], int(room[1]))
    if 'northpole' in room_name:
        print(room_name, room[1])

print('Good room total:' + str(total))
