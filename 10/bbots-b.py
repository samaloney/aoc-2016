from collections import OrderedDict

test_data = '''value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2'''

bots = {}
output = {} #OrderedDict()

lines  = test_data.split('\n')

with open('input.txt') as file:
    lines = file.readlines()

for line in lines:
    instr = line.strip().split(' ')
    if instr[0] == 'value':
        if instr[5] in bots:
            if 'vals' in bots[instr[5]]:
                tmp = bots[instr[5]]['vals']
                tmp.append(int(instr[1]))
                bots[instr[5]]['vals'] = sorted(tmp)
            else:
                bots[instr[5]]['vals'] = [int(instr[1])]
        else:
            bots[instr[5]] = {'vals' : [int(instr[1])] }
        
    elif instr[0] == 'bot':
        if instr[1] in bots:
            bots[instr[1]]['instr'] = instr[2:]
        else:
            bots[instr[1]] = {'instr': instr[2:]}

def ready(bot):
    if 'vals' in bot and 'instr' in bot:
        nvals = len(bot['vals'])
        if nvals == 2:
            return True
    else:
        return False 

while len(bots) > 0:
    cur = ''
    for key, value in bots.items():
        if ready(value):
            cur = key

#    print('Ready: ', cur)

    if cur != '':
        cur_bot = bots.pop(cur)
        instr = cur_bot['instr']
        if instr[3] == 'bot':
            if instr[4] in bots:
                if 'vals' in bots[instr[4]]:
                    tmp = bots[instr[4]]['vals']
                    tmp.append(cur_bot['vals'][0])
                    if tmp == [17,61]:
                        print(instr[3])
#                        break
                    bots[instr[4]]['vals'] = sorted(tmp)
                else:
                    bots[instr[4]]['vals'] = [cur_bot['vals'][0]]

            else:
                bots[instr[4]] = {'vals': [cur_bot['vals'][0]]}

        elif instr[3] == 'output':
           output[instr[4]] = cur_bot['vals'][0]             

        if instr[8] == 'bot':
            if instr[9] in bots:
                if 'vals' in bots[instr[9]]:
                    tmp = bots[instr[9]]['vals']
                    tmp.append(cur_bot['vals'][1])
                    if tmp == [17,61]:
                        print(instr[9])
#                        break
                    bots[instr[9]]['vals'] = sorted(tmp)
                else:
                    bots[instr[9]]['vals'] = [cur_bot['vals'][1]]
            else:
                bots[instr[9]] = {'vals': [cur_bot['vals'][1]]}
        elif instr[3] == 'output':
           output[instr[9]] = cur_bot['vals'][1]             


