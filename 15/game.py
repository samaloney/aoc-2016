import re

data = '''Disc #1 has 5 positions; at time=0, it is at position 4.
Disc #2 has 2 positions; at time=0, it is at position 1.'''

data = '''Disc #1 has 13 positions; at time=0, it is at position 10.
Disc #2 has 17 positions; at time=0, it is at position 15.
Disc #3 has 19 positions; at time=0, it is at position 17.
Disc #4 has 7 positions; at time=0, it is at position 1.
Disc #5 has 5 positions; at time=0, it is at position 0.
Disc #6 has 3 positions; at time=0, it is at position 1.'''

disks = {}
lines = data.split('\n')

for line in lines:
    disk_id = int(re.findall(r'#(\d+)', line)[0])
    n_pos = int(re.findall(r'has (\d+) positions', line)[0])
    pos  = int(re.findall(r'(\d+)\.$', line)[0])
    disks[disk_id] = {'pos': pos, 'n_pos': n_pos}

#part 2
disks[7] = {'pos': 0, 'n_pos':11}

time = 0
while True:
    offset = 1
    tmp = {}
    for disk, props in disks.items():
        aa = (props['pos'] + time + offset) % props['n_pos']
        tmp[disk] = aa
        offset += 1
    
    print(time, tmp)
    if all(map(lambda v: v==0, tmp.values())):
        break

    time += 1
