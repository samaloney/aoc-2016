with open('input.txt') as file:
    data = file.read()

registers = {'a': 7, 'b': 0, 'c': 0, 'd': 0}
out = []

sc = '''cpy 365 b
inc d
dec b
jnz b -2
dec c
jnz c -5'''.split('\n')

def exec_instrs(data):
    cur_instr = 0
    instrs =  data.split('\n')
    instrs = instrs[:-1]
    while cur_instr < len(instrs):
        #print(cur_instr, instrs[cur_instr], registers)
        parts = instrs[cur_instr].split(' ')
        cmd = parts[0]
        
        cur_instr += 1
        if instrs[cur_instr-1:cur_instr - 1 + 6] == sc:
            registers['d'] += 365 * registers['c']
            registers['b'] = 0
            registers['c'] = 0
            cur_instr += 5
            #pass          
        elif cmd == 'cpy':
            if parts[2] in ['a', 'b', 'c', 'd']:
                if parts[1] in ['a', 'b', 'c', 'd']:
                    registers[parts[2]] = registers[parts[1]]
                else:
                    registers[parts[2]] = int(parts[1])
            
        elif cmd == 'inc':
            registers[parts[1]] += 1
        elif cmd == 'dec':
            registers[parts[1]] -= 1
        elif cmd == 'jnz':
            if parts[1] in ['a', 'b', 'c', 'd']:
                if registers[parts[1]] != 0:
                    if parts[2] in ['a', 'b', 'c', 'd']:
                        cur_instr = (cur_instr - 1) + registers[parts[2]]
                    else:
                        cur_instr = (cur_instr - 1) + int(parts[2])
            else:
                if parts[1] != '0':
                    if parts[2] in ['a', 'b', 'c', 'd']:
                        cur_instr = (cur_instr - 1) + registers[parts[2]]
                    else:
                        cur_instr = (cur_instr - 1) + int(parts[2])

        elif cmd == 'out':
            if parts[1] in ['a', 'b', 'c', 'd']:
                out.append(registers[parts[1]])
            else:
                out.append(int(cmd[1]))

            if len(out) > 10:
                break

amin = 0
while True:
    _ = exec_instrs(data)
    zeros = out[::2]
    ones = out[1::2]
    if zeros.count(0) == len(zeros) and ones.count(1) == len(ones):
        break
    amin += 1
    registers['a'] = amin
    out = []
    print(amin)
    
print(registers['a'])
