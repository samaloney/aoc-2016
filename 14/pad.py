from hashlib import md5
import re

import functools

salt = 'yjdafjpo'
#salt = 'abc'
index = 0
count = 0

aaa_re = r'(.)\1\1' 
pad = []

@functools.lru_cache(maxsize=None)
def gen_hash(hsh):
    for i in range(2016):
        hsh = md5(hsh.encode()).hexdigest()
    
    return hsh 

  
while count < 64:
    h = md5((salt+str(index)).encode()).hexdigest()
    h = gen_hash(h)
    a = re.findall(aaa_re, h)
    if a:
        hx = a[0]*5
        for i in range(1, 1001):
            sh = md5((salt+str(index+i)).encode()).hexdigest()
            sh = gen_hash(sh)
            if hx in sh:
                print('Found key: ' + h)
                count += 1
                pad.append((count, index, h))
    
    index += 1

print(index-1)
