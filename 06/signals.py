from collections import defaultdict


test_data = '''eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar'''


with open('input.txt') as file:
    data = file.readlines()

signal = {i: defaultdict(int) for i in range(len(data[0].strip()))}

#for word in test_data.split('\n'):
for word in data:
    for i, char  in enumerate(word.strip()):
           signal[i][char] += 1

out = []

for key, value in signal.items():
    out.append(sorted(value.items(), key=lambda x: -x[1])[0][0])

print('A: ' + ''.join(out))

out = []

for key, value in signal.items():
    out.append(sorted(value.items(), key=lambda x: -x[1])[-1][0])

print('B: '+ ''.join(out))

