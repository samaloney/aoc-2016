
def r(e):
    l = len(e)
    if l % 2 == 0:
        return e[::2]
    elif l % 2 != 0:
        return e[::2][1:]
    else:
        return [e[0]]

e = [x for x in range(1,3017957+1)]

while len(e) > 1:
    e = r(e)

print(e)

target = 3017957
i = 1

while i * 3 < target:
    i *= 3

print(target - i)
