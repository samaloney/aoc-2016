


def is_open(x, y, secret):
    num = x*x + 3*x + 2*x*y + y + y*y + secret
    return bin(num).count('1') % 2 == 0 and x >= 0 and y >= 0

maze = []
for y in range(7):
    maze.append(['#.'[is_open(x,y, 10)] for x in range(10)])

maze[1][1] = 'O'

def pm(maze):
    for line in maze:
        print(''.join(line))


pm(maze)

frontier = [(1,1, 0)]
expolored = {}

while len(frontier) > 0:
    x, y, d = frontier.pop()
    expolored[(x, y)] = d
    d += 1
    possibles = ((x+1, y, d), (x-1, y, d), (x, y+1, d), (x, y-1, d))
#    possibles = sorted(possibles, key=lambda x: ((x[0]-target[0])+(x[1]-target[1])))
    for pos in possibles:
        if is_open(pos[0], pos[1], 1350):
            if (pos[0], pos[1]) not in expolored or expolored[(pos[0], pos[1])] > pos[2]:
                frontier.append(pos)

print(expolored[(31,39)], len([1 for v in expolored.values() if v <= 50]) )
