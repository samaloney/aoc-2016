from collections import deque
from itertools import permutations


with open('input.txt') as file:
    data = file.readlines()
    data = [line.strip() for line in data]

data = [x for x in data]

locs = []

for y, row in enumerate(data):
    for x, col in enumerate(row):
        if col.isdigit():
            locs.append((x, y,int(col)))

locs = sorted(locs, key=lambda x: x[2])
orig = locs[0]
size = (len(data[0]), len(data))

def search(loc):
    frontier = [loc]
    explored = {}
    while len(frontier) > 0:
        x, y, d = frontier.pop()
        d += 1
        explored[(x, y)] = d
        possibles = ((x+1, y, d), (x-1, y, d), (x, y+1, d), (x, y-1, d))
        for pos in possibles:
            x, y, d  = pos
            if x >= 0 and x < size[0] and y >= 0 and y < size[1] and data[y][x] != '#':
                if (x, y) not in explored or explored[(x, y)] > d:
                    frontier.append(pos)
    
    return explored


def bfs(orig, dest):
    q  = deque([(orig, 0)])
    vis = set([orig])
    while q:
        cur, dst = q.pop()
        if cur == dest:
            return dst
        x, y = cur
        dst += 1
        possibles = possibles = ((x+1, y), (x-1, y), (x, y+1), (x, y-1))
        for pos in possibles:
            x, y = pos
            if data[y][x] != '#' and pos not in vis:
                q.appendleft((pos, dst))
                vis.add(pos)

    return -1

orig = (locs[0][0], locs[0][1])
dst_0 = [ bfs(orig, (x, y)) for x, y, _ in locs[1:]]
k = len(locs) -1
dsts = [[None for j in range(k)] for i in range(k)]
pos = [(x, y) for x, y, _ in locs[1:]]

for i in range(k):
    for j in range(k):
        dsts[j][i] = dsts[i][j] = bfs(pos[i], pos[j])

part1, part2 = 1e12, 1e12
for path in permutations(range(k)):
    dst = dst_0[path[0]]
    for i in range(len(path)-1):
        dst += dsts[path[i]][path[i+1]]
    part1 = min(part1, dst)
    dst += dst_0[path[-1]]
    part2 = min(part2, dst)

print(part1, part2)
