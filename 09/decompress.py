import re

data = [['ADVENT','ADVENT', 6],
        ['A(1x5)BC', 'ABBBBBC', 7],
        ['(6x1)(1x3)A', '(1x3)A', 6],
        ['X(8x2)(3x3)ABCY', 'X(3x3)ABC(3x3)ABCY', 18]]


marker_re = r'\((\d)x(\d)\)'


def decompress(data):
    out = []
    last_end = 0
    last_span = 0
    markers = re.finditer(marker_re, data)
    for marker in markers:
        start, end = marker.start(), marker.end()
        span,  mul, = int(data[start + 1]), int(data[end -2])
        if end + span > last_end + last_span:
            out.extend(data[last_end:start])
            out.extend(data[end:end + span] * mul)
            last_end = end
            last_span = span
#            import ipdb; ipdb.set_trace()
    
    if len(out) == 0:
        return data 
    elif last_end + last_span < len(data):
        out.extend(data[last_end+last_span:])
    
    return ''.join(out)
        
def dl(s, part2=False):
    if '(' not in s:
        return len(s)
    ret = 0
    while '(' in s:
        ret += s.find('(')
        s = s[s.find('('):]
        marker = s[1:s.find(')')].split('x')
        s = s[s.find(')') + 1:]
        if part2:
            ret += dl(s[:int(marker[0])], part2=True) * int(marker[1])
        else:
            ret += len(s[:int(marker[0])]) * int(marker[1])
        s = s[int(marker[0]):]

    ret += len(s)
    return ret

for test in data:
    res = decompress(test[0])
    l = dl(test[0])
    if res != test[1] and len(res) != test[2]:
        print('Fail: ', res)
    
    if l != test[2]:
        print('Fail: ', l, test[2])

with open('input.txt') as file:
    lines= file.read()


res1 = dl(lines.strip())
res2 = dl(lines.strip(), part2=True)
print(res1, res2)

