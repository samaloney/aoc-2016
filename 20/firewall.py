import re

data = '''5-8
0-2
4-7'''

with open('input.txt') as file:
    data = file.read()

rules = sorted(re.findall(r'(\d+)-(\d+)', data))
rules = [(int(x), int(y)) for x, y in rules]
rules.sort()


def test_ip(ip):
    for lo, hi in rules:
        if lo <= ip <= hi:
            break
    else:
        if ip < 2**32:
            return True
    return False


cand = [x[1] + 1 for x in rules]

valid = [c for c in cand if test_ip(c)]

print(valid[0])

total = 0
for ip in valid:
    while test_ip(ip):
        total += 1
        ip += 1

print(total)
