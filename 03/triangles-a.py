import re


with open('input.txt') as file:
  data = file.read()

pattern = r'\s(\d+)\s+(\d+)\s+(\d+)'

triangle_specs = re.findall(r'\s(\d+)\s+(\d+)\s+(\d+)', data)


def triangle_test1(specs):
    count = 0
    for triangle in triangle_specs:
        a, b, c = sorted([ int(side) for side in triangle])
        if  (a + b) > c: 
            #print(a, b, c, 'True')
            count += 1
        #else:
            #print(a, b, c, 'False')
    
    return count

def triangle_test2(specs):
    count = 0
    for triangle in specs:
        a, b, c = [ int(side) for side in triangle]
        if  (a + b) > c and (a + c) > b and (c + b) > a: 
            print(a, b, c, 'True')
            count += 1
        else:
            print(a, b, c, 'False')
    
    return count

print('Pint A: ' + str(triangle_test2(triangle_specs)))

new_spec = []
for t1, t2, t3, in zip(*[iter(triangle_specs)]*3):
    for i in range(3):
        new_spec.append([t1[i], t2[i], t3[i]])

print('Pint B: ' + str(triangle_test2(new_spec)))
