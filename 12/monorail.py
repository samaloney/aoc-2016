data = '''cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a'''


with open('input.txt') as file:
    data = file.read()


registers = {'a': 0, 'b': 0, 'c': 0, 'd': 0}


def exec_instrs(data):
    cur_instr = 0
    instrs =  data.split('\n')
    while cur_instr < len(instrs):
        parts = instrs[cur_instr].split(' ')
        cmd = parts[0]
        
        cur_instr += 1

        if cmd == 'cpy':
            if parts[1] in ['a', 'b', 'c', 'd']:
                registers[parts[2]] = registers[parts[1]]
            else:
                registers[parts[2]] = int(parts[1])
            
        elif cmd == 'inc':
            registers[parts[1]] += 1
        elif cmd == 'dec':
            registers[parts[1]] -= 1
        elif cmd == 'jnz':
            if parts[1] in ['a', 'b', 'c', 'd']:
                if registers[parts[1]] != 0:
                    cur_instr = (cur_instr - 1) + int(parts[2])
            else:
                if parts[1] != '0':
                    cur_instr = (cur_instr - 1) + int(parts[2])


exec_instrs(data)

print(registers['a'])

registers = {'a': 0, 'b': 0, 'c': 1, 'd': 0}

exec_instrs(data)

print(registers['a'])
