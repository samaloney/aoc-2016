

def is_trap(chars):
    state = False
    if chars[0:2] == '^^' and chars[2] == '.':
        state = True
    elif chars[1:3] == '^^' and chars[0] == '.':
        state = True
    elif chars[0] == '^' and chars[1:3] == '..':
        state = True
    elif chars[2] == '^' and chars[0:2] == '..':
        state = True

    return state


def gen_row(row):
    out = ['']*len(row)
    for i, char in enumerate(row):
        if i == 0:
            last = '.' + row[i:i+2]
            if is_trap(last):
                out[i] = '^'
            else:
                out[i] = '.'
        elif i == len(row) - 1:
            last = row[-2:] + '.'
            if is_trap(last):
                out[i] = '^'
            else:
                out[i] = '.'
        else:   
            last = row[i-1:i+2]
            if is_trap(last):
                out[i] = '^'
            else:
                out[i] = '.'
    
    return ''.join(out)


def gen_tiles(start, nrow):
    out = [start]
    for i in range(1,nrow):
        out.append(gen_row(out[i-1]))

    return out

st = '^.^^^.^..^....^^....^^^^.^^.^...^^.^.^^.^^.^^..^.^...^.^..^.^^.^..^.....^^^.^.^^^..^^...^^^...^...^.'
rows = 40

tiles = gen_tiles(st, 400000)
print(''.join(tiles).count('.'))
