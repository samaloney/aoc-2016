import numpy as np

data='''rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1'''

def parse_intruction(instr):
    parts = instr.split(' ')
    if parts[0] == 'rect':
        a, b = parts[1].split('x')
        rect(int(a), int(b))
    elif parts[0] == 'rotate':
        col_or_row = parts[2].split('=')[1]
        num = parts[4]
        if parts[1] == 'row':
            rotate_row(int(col_or_row), int(num))
        elif parts[1] == 'column':
            rotate_col(int(col_or_row), int(num))
    

def rect(a, b):
    display[0:a, 0:b] = 1

def rotate_col(col, num):
    display[col,:] = np.roll(display[col,:], num)

def rotate_row(row, num):
    display[:,row] = np.roll(display[:,row], num)

with open('input.txt') as file:
    data  = file.read()


display = np.zeros([50,6],dtype=int)

for line in data.split('\n'):
    parse_intruction(line)

print(np.sum(display))

for i in range(10):
    print(display[i*5:(i*5)+5,:].transpose())

print('ZFHFSFOGPO')

