import re

#data = '''abba[mnop]qrst
#abcd[bddb]xyyx
#aaaa[qwer]tyui
#ioxxoj[asdfgh]zxcvbn'''

with open('input.txt') as file:
    data = file.read()

ip_re = r'\[\w+\]'
hypernet_re = r'\[(\w+)\]'
abba_re = r'(\w)(\w)\2\1'

def is_abba(ip):
    hypernets = re.findall(hypernet_re, ip)
    hn_abbas = map(lambda x:re.findall(abba_re, x), hypernets)
    if not any(hn_abbas):
        ip_parts = re.split(ip_re, ip)
        for part in ip_parts:
            match = re.findall(abba_re, part)
            if match and match[0][0] != match[0][1]:
                print(ip)
                return 1
    
    return 0

ips = data.split('\n')

good_ip_count = 0
for ip in ips:
    good_ip_count += is_abba(ip)

print(good_ip_count)
