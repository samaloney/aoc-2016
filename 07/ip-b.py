import re

#data = '''aba[bab]xyz
#xyx[xyx]xyx
#aaa[kek]eke
#zazbz[bzb]cdb'''

with open('input.txt') as file:
    data = file.read()

ip_re = r'\[\w+\]'
hypernet_re = r'\[(\w+)\]'
aba_re = r'(?=(\w)(\w)\1)'

def is_abba(ip_addr):
    ip_parts = re.split(ip_re, ip_addr)
    hypernets = re.findall(hypernet_re, ip_addr)
    for ip in ip_parts:
        abas = re.findall(aba_re, ip)
        for aba in abas:
            bab = aba[1] + aba[0] + aba[1]
            for hpnet in hypernets:
                if bab in hpnet:
#                    print('True: ', ip_addr)
                    return 1

#    print('False: ', ip_addr)
    return 0

ips = data.split('\n')

good_ip_count = 0
for ip in ips:
    good_ip_count += is_abba(ip)

print(good_ip_count)
